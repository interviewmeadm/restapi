**myReatil REST API**

=======================================================
**Setup**: This project was written in Scala + Spray using AKKA Framework, for database Mongo is used in embedded form, all the components for this project are in built so coul be used either Unix or Windows env

To run the code execute the sbt.retail.bat file in the /tool/sbt/bin folder. The following are commonly used commands:

**gen-idea**			 - generate intellij project

**test**				   - run regular tests

**compile**				 - compile the code

**pack** 					 - create package to run either windows or Unix, componen found under sources\server\retail-container\target

**Requirement**:

Java 1.6 or later

Internet connection

Free ports: 8080 and 27017

Running under target folder goto to the bin folder and execute container.bat (windows) or container (Unix)

Running through Intellij open the project after running gen-idea command in sbt and open Container.scala; this is the main class, right click and run

For first time the project will download embedded mongo, be patient :)

To run sbt simply under sources\server run ..\..\tools\sbt\bin\sbt.retail.bat this will prompt sbt and you will be able to run the commands

The project is using port 8080 to expose the API with the following routes: (Important use 127.0.0.1 instead of localhost)

**127.0.0.1:8080/products/{productId}**  ---> GET

**127.0.0.1:8080/products/{productId}**  ---> PUT (this is upsert)
with body raw text like

{

  "id": 50,

  "name": "NewProduct",

  "currentPrice": {

    "value": 150,

    "currency_code": "PESOS"

  }
}

**127.0.0.1:8080/initialize**    --> insert 100 elements

While app is running you can connect through Robomongo using port 27017 and see the database. When the app is shutdown data will be destroyed

Junit were written using ScalaTest