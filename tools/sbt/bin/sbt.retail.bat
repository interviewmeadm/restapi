@echo off

set SBT_HOME=%~dp0
set JAVA_OPTS=-Dsbt.ivy.home=D:\ivyrepo -Dsbt.boot.properties="sbt.boot.retail.properties" -Xms128M -Xmx1024M -XX:MaxPermSize=768m

java %JAVA_OPTS% -jar "%SBT_HOME%sbt-launch.jar" %*