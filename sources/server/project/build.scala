import sbt.Keys._
import sbt._
import xerial.sbt.Pack._

object BuildSettings {
  val buildOrganization = "com.target.retai"
  val buildScalaVersion = "2.10.5"

  val versionIncrement = "0.0.1"
  val dependencySuffix = "-SNAPSHOT"

  val DEPENDENCY_VERSION = s"$versionIncrement$dependencySuffix"

  val buildSettings = Defaults.coreDefaultSettings ++ Seq(
    organization := buildOrganization,
    scalaVersion := buildScalaVersion,
    scalacOptions := Seq(
      "-encoding", "UTF-8",
      "-deprecation",
      "-unchecked",
      "-optimize",
      "-Xlint"),
    javacOptions := Seq(
      "-encoding", "UTF-8",
      "-Xlint:unchecked",
      "-Xlint:deprecation")
  )

  lazy val retailPackSettings = packAutoSettings ++
      packZipSettings ++
      Seq(packGenerateWindowsBatFile := true) ++ publishPackArchiveTgz

  lazy val packZip = taskKey[File]("Creates zip from pack")
  lazy val packZipSettings = Seq[Def.Setting[_]](
    packZip := {
      val zipFile = target.toTask.value / s"retail-${version.toTask.value}.zip"
      val packDir: File = pack.toTask.value
      IO.zip(Path.allSubpaths(packDir), zipFile)
      zipFile
    }
  )
}
  

object ShellPrompt {

  object devnull extends ProcessLogger {
    def info(s: => String) {}

    def error(s: => String) {}

    def buffer[T](f: => T): T = f
  }

  val versionKey: SettingKey[String] = version in ThisBuild

  val buildShellPrompt = {
    (state: State) => {
      val extract: Extracted = Project.extract(state)
      val versionName: String = extract.get(versionKey)
      val currProject = Project.extract(state).currentProject.id
      "%s:%s> ".format(currProject, versionName)
    }
  }
}

object Versions {
  val EmbedMongoVersion = "1.50.3"
  val AkkaVersion = "2.3.10"
  val SprayJSONVersion = "1.3.2"
  val JSON4sVersion = "3.2.10"
  val SprayVersion = "1.3.3"
  val ReactiveMongoVersion = "0.11.11"
}

object Dependencies {

  import BuildSettings.buildScalaVersion
  import Versions._

  lazy val reactiveMongo = "org.reactivemongo" %% "reactivemongo" % ReactiveMongoVersion
	lazy val embedMongo = "de.flapdoodle.embed" % "de.flapdoodle.embed.mongo" % EmbedMongoVersion
	
  lazy val sprayCan = "io.spray" %% "spray-can" % SprayVersion
  lazy val sprayRouting = "io.spray" %% "spray-routing" % SprayVersion
  lazy val sprayTestkit = "io.spray" %% "spray-testkit" % SprayVersion
  lazy val sprayJson = "io.spray" %% "spray-json" % SprayJSONVersion
  lazy val json4s = "org.json4s" %% "json4s-native" % JSON4sVersion
  lazy val json4sJackson = "org.json4s" %% "json4s-jackson" % JSON4sVersion
  lazy val scalaReflect = "org.scala-lang" % "scala-reflect" % buildScalaVersion
  lazy val scalap = "org.scala-lang" % "scalap" % buildScalaVersion
  
  lazy val akkaActor = "com.typesafe.akka" %% "akka-actor" % AkkaVersion
  lazy val akkaTestkit = "com.typesafe.akka" %% "akka-testkit" % AkkaVersion
  lazy val akkaSlf4J = "com.typesafe.akka" %% "akka-slf4j" % AkkaVersion
  lazy val akkaContrib = "com.typesafe.akka" %% "akka-contrib" % AkkaVersion


  lazy val scalatest = "org.scalatest" %% "scalatest" % "2.1.0"

  lazy val logback = "ch.qos.logback" % "logback-classic" % "1.0.13"


  def compile(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "compile")

  def tests(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

  def integrationTests(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "it")

  def runtime(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "runtime")
}

object Retail extends Build {

  import BuildSettings._
  import Dependencies._

  testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-oI")

  lazy val commonDeps = Seq(scalatest, akkaTestkit, sprayTestkit)

  override lazy val settings = super.settings :+ {
    shellPrompt := ShellPrompt.buildShellPrompt
  }

  lazy val retail = Project("retail", file("."))
    .aggregate(retailContainer)
    .settings(buildSettings: _*)
  
  lazy val retailContainer = Project("retail-container", file("retail-container"))
    .settings(buildSettings: _*)
    .settings(retailPackSettings: _*)
    .settings(libraryDependencies ++=
    compile(akkaActor, json4s, json4sJackson, sprayCan, sprayRouting, sprayJson, embedMongo, akkaSlf4J, akkaContrib, logback, reactiveMongo) ++
      tests(commonDeps: _*))
    
  
}
