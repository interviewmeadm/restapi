libraryDependencies += "de.flapdoodle.embed" % "de.flapdoodle.embed.mongo" % "1.44"

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.6.0")

addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.7.9")

addSbtPlugin("io.spray" % "sbt-revolver" % "0.7.1")