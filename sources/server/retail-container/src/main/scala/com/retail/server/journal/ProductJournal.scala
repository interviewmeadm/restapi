package com.retail.server.journal

import akka.actor.{Actor, Props}
import akka.pattern._
import com.retail.server.messages._
import reactivemongo.api.commands.UpdateWriteResult
import reactivemongo.bson.BSONDocument

import scala.concurrent.Future
import scala.util.Random

object ProductJournal {
  def props = Props[ProductJournal]
}

class ProductJournal extends Actor with MongoConnection {
  import com.retail.server.journal.Formats._

  override def preStart(): Unit = {
    super.preStart()
    connect()
  }

  import scala.concurrent.ExecutionContext.Implicits.global

  override def receive: Receive = handleRequest orElse handleOther

  def handleRequest: Receive = {
    case msg: GetProductInfo =>
      val replyTo = sender()

      val f: Future[List[GetProductResponse]] = collection.find(BSONDocument("id" -> msg.id)).cursor[GetProductResponse].collect[List]()

      f.map {
        case result =>
          result.headOption.getOrElse(GetProductResponse(9999, "EMPTY",CurrentPrice(0,"NOT_FOUND")))
      } recover {
        case failure =>
          GetProductResponse(9999, failure.getMessage,CurrentPrice(0,"DB_ERROR"))
      } pipeTo replyTo

    case msg: PutProductInfo =>
      val replyTo = sender()
      val f: Future[UpdateWriteResult] = collection.update(BSONDocument("id" -> msg.productInfo.id),msg.productInfo, upsert = true)
      f.map {
        case result =>
          PutProductResponse("Upsert was successful")
      } recover {
        case failure =>
          failure.printStackTrace()
          PutProductResponse(s"Upsert failed because: ${failure.getMessage}")
      } pipeTo replyTo
  }

  def handleOther: Receive = {
    case 'Initialize =>
      (1 to 100).map {
        e =>
          val currency = List("USD", "MXN", "EUR", "CAN")
         collection.insert(GetProductResponse(e, s"Product$e", CurrentPrice(Random.nextLong(), currency.lift(Random.nextInt(currency.size)).getOrElse("USD"))))
      }
  }
}
