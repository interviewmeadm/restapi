package com.retail.server.journal

import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.{MongoConnectionOptions, MongoDriver}

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

trait MongoConnection {

  var collection: BSONCollection  = _

  def connect() = {
    val timeout = 10.seconds
    lazy val driver = new MongoDriver
    lazy val connection = driver.connection(
      List("localhost:27017"), MongoConnectionOptions())

    val db = {
      val _db = connection("retail")
      Await.ready(_db.drop, timeout)
      _db
    }

    collection = db.collection("products")
  }

}
