package com.retail.server.handler

import akka.actor.{Actor, ActorRef, Props}
import com.retail.server.messages.{GetProductInfo, GetProductResponse}
import spray.http.StatusCodes
import spray.routing.RequestContext

object ProductInfoGetHandler {
  def props(ctx: RequestContext, productId: Long, journal: ActorRef) = Props(new ProductInfoGetHandler(ctx, productId, journal))
}

class ProductInfoGetHandler(ctx: RequestContext, productId: Long, journal: ActorRef) extends Actor with RequestResponse{

  override def preStart(): Unit = {
    super.preStart()
    journal ! GetProductInfo(productId)
  }

  override def receive: Receive = handleApiRequest

  def handleApiRequest: Receive = {
    case msg: GetProductResponse =>
      complete(StatusCodes.OK, Some(msg))
  }

  override def requestContext: RequestContext = ctx
}
