package com.retail.server.handler

import akka.actor.{Actor, ActorRef, Props}
import com.retail.server.messages.{GetProductResponse, PutProductInfo, PutProductResponse}
import spray.http.StatusCodes
import spray.routing.RequestContext


object ProductInfoPutHandler {
  def props(ctx: RequestContext, journal: ActorRef, productInfo: GetProductResponse) = Props(new ProductInfoPutHandler(ctx, journal, productInfo))
}

class ProductInfoPutHandler(ctx: RequestContext, journal: ActorRef, productInfo: GetProductResponse) extends Actor with RequestResponse{

  override def preStart(): Unit = {
    super.preStart()
    journal ! PutProductInfo(productInfo)
  }

  override def receive: Receive = handleDBResults

  def handleDBResults: Receive = {
    case msg: PutProductResponse =>
      complete(StatusCodes.OK, Some(msg.result))
  }

  override def requestContext: RequestContext = ctx
}
