package com.retail.server.handler

import akka.actor.Actor
import com.retail.server.marshall.ProductMarshaller
import org.json4s.{DefaultFormats, Formats}
import spray.http.StatusCode
import spray.routing.RequestContext

trait RequestResponse extends ProductMarshaller{
  this: Actor =>

  override implicit def json4sFormats: Formats = DefaultFormats

  import context._

  def requestContext : RequestContext

  def complete[T <: AnyRef](statusCode: StatusCode, obj: Option[T]) = {
    requestContext.complete((statusCode,obj.getOrElse(None)))
    stop(self)
  }
}
