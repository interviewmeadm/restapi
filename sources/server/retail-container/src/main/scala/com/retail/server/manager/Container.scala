package com.retail.server.manager

import akka.actor.{ActorRef, ActorSystem}
import akka.io.IO
import com.retail.server.handler.RestServiceHandler
import com.retail.server.journal.{EmbeddedMongo, ProductJournal}
import spray.can.Http

object Container  {
  implicit val system = ActorSystem("RetailAPI")
  def main(args: Array[String]) {

  val embeddedMongo = new EmbeddedMongo
  embeddedMongo.startEmbedded

  val journal = system.actorOf(ProductJournal.props, "Journal")

  val handler = system.actorOf(RestServiceHandler.props(journal), "RestHandler")

    println(s"$journal - $handler")

  IO(Http) ! Http.Bind(handler, interface = "localhost", port = 8080)
  }
}
