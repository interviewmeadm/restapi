package com.retail.server.messages

sealed trait RetailMessages

case class GetProductInfo(id: Long) extends RetailMessages
case class PutProductInfo(productInfo: GetProductResponse) extends RetailMessages

case class CurrentPrice(value: Long, currency_code: String)
case class GetProductResponse(id: Long, name: String, currentPrice: CurrentPrice)
case class PutProductResponse(result: String)
