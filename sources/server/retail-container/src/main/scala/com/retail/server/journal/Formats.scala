package com.retail.server.journal

import com.retail.server.messages.{CurrentPrice, GetProductResponse}
import reactivemongo.bson.Macros

object Formats {
  implicit val bsonCurrentPrice = Macros.handler[CurrentPrice]
  implicit val bsonProduct = Macros.handler[GetProductResponse]
}
