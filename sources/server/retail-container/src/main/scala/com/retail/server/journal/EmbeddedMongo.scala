package com.retail.server.journal

import java.util

import de.flapdoodle.embed.mongo.{Command, MongodExecutable, MongodStarter}
import de.flapdoodle.embed.mongo.config.{ArtifactStoreBuilder, DownloadConfigBuilder, _}
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.process.config.io.ProcessOutput
import de.flapdoodle.embed.process.distribution.Distribution
import de.flapdoodle.embed.process.extract.UUIDTempNaming
import de.flapdoodle.embed.process.io.directories.FixedPath
import de.flapdoodle.embed.process.io.{NullProcessor, Processors}
import de.flapdoodle.embed.process.runtime.{ICommandLinePostProcessor, Network}
import de.flapdoodle.embed.process.store.Downloader

class EmbeddedMongo {
  val mongodConfig: IMongodConfig = new MongodConfigBuilder()
    .version(Version.V2_6_11)
    .net(new Net(27017, Network.localhostIsIPv6()))
    .timeout(new Timeout(30000))
    .build()

  val command = Command.MongoD

  val processOutput = new ProcessOutput(new NullProcessor,
    Processors.namedConsole("[MONGOD>]"), Processors.namedConsole("[console>]"))

  val artifactStorePath = new FixedPath("../../mongodb")

  val tempDir           = new FixedPath(System.getProperty("java.io.tmpdir"))

  val commandLinePostProcessor: ICommandLinePostProcessor = new ICommandLinePostProcessor() {
    def process(distribution: Distribution, args: util.List[String]) = {
      args.remove("--noauth")
      args.add("--auth")
      args
    }
  }

  val runtimeConfig = new RuntimeConfigBuilder()
    .defaults(command)
    .commandLinePostProcessor(commandLinePostProcessor)
    .processOutput(processOutput)
    .artifactStore(new ArtifactStoreBuilder()
      .useCache(false)
      .downloader(new Downloader())
      .download(new DownloadConfigBuilder()
        .defaultsForCommand(command)
        .artifactStorePath(artifactStorePath))
      .tempDir(tempDir)
      .executableNaming(new UUIDTempNaming)).build()

  var mongodExecutable: MongodExecutable = _

  val runtime: MongodStarter = MongodStarter.getInstance(runtimeConfig)

  mongodExecutable = runtime.prepare(mongodConfig)

  def startEmbedded = mongodExecutable.start()

  def stopEmbedded = mongodExecutable.stop()

}
