package com.retail.server.handler

import java.util.UUID

import akka.actor.{ActorRef, Props}
import com.retail.server.messages.GetProductResponse
import com.retail.server.routes.ProductService
import spray.http.StatusCodes
import spray.routing.{HttpServiceActor, RequestContext}

object RestServiceHandler {
  def props(journal: ActorRef) = Props(new RestServiceHandler(journal))
}

class RestServiceHandler(journal: ActorRef) extends HttpServiceActor with ProductService{
  def receive: Receive = runRoute(productRoute)

  override def getProductInfo(ctx: RequestContext, productId: Long): Unit = {
    context.actorOf(ProductInfoGetHandler.props(ctx, productId, journal), UUID.randomUUID().toString)
  }

  override def insertData(ctx: RequestContext): Unit = {
    journal ! 'Initialize
    ctx.complete(StatusCodes.OK)
  }

  override def putProductInfo(ctx: RequestContext, productInfo: GetProductResponse): Unit = {
    context.actorOf(ProductInfoPutHandler.props(ctx, journal, productInfo), UUID.randomUUID().toString)
  }
}
