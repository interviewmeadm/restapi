package com.retail.server.routes

import com.retail.server.messages.GetProductResponse
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods._
import spray.http.StatusCodes
import spray.routing.{Directives, RequestContext}

trait ProductService extends Directives{

  implicit def json4sFormats = DefaultFormats

  def getProductInfo(ctx: RequestContext, productId: Long): Unit
  def putProductInfo(ctx: RequestContext, productInfo: GetProductResponse): Unit
  def insertData(ctx: RequestContext): Unit

  val productRoute = {
    path("products" / LongNumber) {
      (productId) =>
        get {
          ctx =>
            getProductInfo(ctx, productId)
        } ~
          put {
            entity(as[String]) {
              productInfo =>
                ctx =>
                if(productInfo.isEmpty)
                  ctx.complete(StatusCodes.BadRequest, "Product Info is missing")
                else {
                  val jsonObject = parse(productInfo)
                  val putProduct = jsonObject.extract[GetProductResponse]

                  if(putProduct.id != productId)
                    ctx.complete(StatusCodes.BadRequest, s"Cannot change product Id original $productId - new ${putProduct.id}")
                  else
                    putProductInfo(ctx, putProduct)
                }
            }
          }
    } ~ path("initialize") {
      get {
        ctx =>
          insertData(ctx)
      }
    }
  }
}
