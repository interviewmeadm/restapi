package com.retail.server.marshall

import java.lang.reflect.InvocationTargetException

import org.json4s.native.Serialization
import org.json4s.{Formats, MappingException}
import spray.http.{ContentType, _}
import spray.httpx.marshalling.Marshaller
import spray.httpx.unmarshalling.Unmarshaller
import spray.json.DefaultJsonProtocol

trait ProductMarshaller extends DefaultJsonProtocol {
  implicit def json4sFormats: Formats
  def serialization = Serialization

  implicit def json4sUnmarshaller[T: Manifest] = {

    Unmarshaller[T](ContentTypeRange(MediaTypes.register(MediaTypes.`application/json`)), ContentTypeRange(MediaTypes.register(MediaTypes.`application/json`))) {
      case x: HttpEntity.NonEmpty ⇒
        try serialization.read[T](x.asString(defaultCharset = HttpCharsets.`UTF-8`))
        catch {
          case MappingException("unknown error", ite: InvocationTargetException) ⇒ throw ite.getCause
        }
    }
  }

  implicit def json4sMarshaller[T <: AnyRef] = {
    Marshaller.delegate[T, String](ContentType(MediaTypes.`application/json`), ContentType(MediaTypes.`application/json`))(serialization.write(_))
  }
}
