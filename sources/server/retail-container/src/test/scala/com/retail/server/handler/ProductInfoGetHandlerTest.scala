package com.retail.server.handler

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import com.retail.server.messages.{CurrentPrice, GetProductInfo, GetProductResponse}
import org.scalatest.{BeforeAndAfterAll, FunSuiteLike}
import spray.http.{HttpCharsets, HttpRequest, HttpResponse, Uri}
import spray.routing.RequestContext

import scala.concurrent.duration._

class ProductInfoGetHandlerTest extends TestKit(ActorSystem("ProductInfoGetHandlerTest")) with FunSuiteLike with BeforeAndAfterAll{

  val dummyRequest: HttpRequest = new HttpRequest()
  val responder = TestProbe()
  val ctx = new RequestContext(dummyRequest, responder.ref, Uri.Path./)
  val journal = TestProbe()
  val maxWait = 3.seconds
  val expectedProduct = 100
  var actorUnderTest: TestActorRef[ProductInfoGetHandler] = _

  override def beforeAll() = {
    actorUnderTest = TestActorRef[ProductInfoGetHandler](ProductInfoGetHandler.props(ctx, expectedProduct, journal.ref))
  }

  override def afterAll() ={
    system.shutdown()
  }

  test("When Actor is created message is sent to the Journal to request the product information") {
    val res = journal.expectMsgClass(maxWait, classOf[GetProductInfo])

    assert(expectedProduct === res.id)
  }

  test("When receive response from journal then complete the request") {
    import org.json4s.DefaultFormats
    import org.json4s.jackson.JsonMethods._

    implicit def json4sFormats = DefaultFormats

    val msg = GetProductResponse(expectedProduct, "SomeName", CurrentPrice(100, "USD"))

    actorUnderTest ! msg

    val httpResponse = responder.expectMsgClass(maxWait, classOf[HttpResponse])
    val entity = httpResponse.entity.asString(HttpCharsets.`UTF-8`)

    val jsonObject = parse(entity)
    val entityResponse = jsonObject.extract[GetProductResponse]
    assert(msg === entityResponse)
  }
}
