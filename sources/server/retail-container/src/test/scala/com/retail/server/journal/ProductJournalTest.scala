package com.retail.server.journal

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import com.retail.server.messages._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuiteLike}

import scala.concurrent.Await
import scala.concurrent.duration._

class ProductJournalTest extends TestKit(ActorSystem("ProductJournalTest")) with FunSuiteLike with BeforeAndAfterAll with BeforeAndAfterEach with ImplicitSender {
  import scala.concurrent.ExecutionContext.Implicits.global

  var actorUnderTest: TestActorRef[ProductJournal] = _
  val maxWait = 5.seconds
  val embeddedMongo = new EmbeddedMongo

  override def beforeAll() = {
    embeddedMongo.startEmbedded
    actorUnderTest = TestActorRef[ProductJournal](ProductJournal.props)
    assert(actorUnderTest.underlyingActor.collection.name === "products")
  }

  override def afterAll() ={
    embeddedMongo.stopEmbedded
    system.shutdown()
  }

  test("When Initialize is received then 100 values are inserted in DB") {
    actorUnderTest ! 'Initialize

    expectNoMsg(maxWait)

    val count = Await.result(actorUnderTest.underlyingActor.collection.count(), maxWait)

    assert(100 === count)
  }

  test("When a product is requested and exists in db then return it") {
    val expectedProduct = 24
    actorUnderTest ! GetProductInfo(expectedProduct)

    val res = expectMsgClass(maxWait, classOf[GetProductResponse])

    assert(expectedProduct === res.id)
  }

  test("When a product is requested and it does not exists a general product is returned instead"){
    val expectedProduct = GetProductResponse(9999, "EMPTY",CurrentPrice(0,"NOT_FOUND"))
    actorUnderTest ! GetProductInfo(1976)

    val res = expectMsgClass(maxWait, classOf[GetProductResponse])

    assert(expectedProduct === res)
  }

  test("When Put is called and the product does not exist then it is inserted"){
    var expectedProduct = GetProductResponse(9999, "EMPTY",CurrentPrice(0,"NOT_FOUND"))
    actorUnderTest ! GetProductInfo(1976)

    var res = expectMsgClass(maxWait, classOf[GetProductResponse])

    assert(expectedProduct === res)

    expectedProduct = GetProductResponse(1976, "Some", CurrentPrice(110, "Currency"))
    actorUnderTest ! PutProductInfo(expectedProduct)
    val putResponse = expectMsgClass(maxWait, classOf[PutProductResponse])

    assert(PutProductResponse("Upsert was successful") === putResponse)

    actorUnderTest ! GetProductInfo(1976)

     res = expectMsgClass(maxWait, classOf[GetProductResponse])

    assert(expectedProduct === res)
  }

  test("When Put is called and the product exists then update it"){
    val productId = 50
    actorUnderTest ! GetProductInfo(productId)

    var res = expectMsgClass(maxWait, classOf[GetProductResponse])

    assert(productId === res.id)

    val expectedProduct = GetProductResponse(productId, "NewProduct", CurrentPrice(98765, "Free"))
    actorUnderTest ! PutProductInfo(expectedProduct)
    val putResponse = expectMsgClass(maxWait, classOf[PutProductResponse])

    assert(PutProductResponse("Upsert was successful") === putResponse)

    actorUnderTest ! GetProductInfo(productId)

    res = expectMsgClass(maxWait, classOf[GetProductResponse])

    assert(expectedProduct === res)
  }

}
