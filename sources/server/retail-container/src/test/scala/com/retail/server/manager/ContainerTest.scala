package com.retail.server.manager

import java.net.Socket

import akka.util.Timeout
import org.scalatest.FunSuite

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Try

class ContainerTest extends FunSuite {

  test("Validate IO bounds correctly and actors are created"){
    implicit val timeout: Timeout = 5.seconds

    Container.main(Array.empty[String])

    Await.result(Container.system.actorSelection("user/RestHandler").resolveOne(), timeout.duration)
    Await.result(Container.system.actorSelection("user/Journal").resolveOne(), timeout.duration)

    assert(Try(new Socket("127.0.0.1", 8080).close()).isSuccess)
    assert(Try(new Socket("127.0.0.1", 27017).close()).isSuccess)
  }

}
