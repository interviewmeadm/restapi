package com.retail.server.handler

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import com.retail.server.messages._
import org.scalatest.{BeforeAndAfterAll, FunSuite, FunSuiteLike}
import spray.http.{HttpCharsets, HttpRequest, HttpResponse, Uri}
import spray.routing.RequestContext

import scala.concurrent.duration._

class ProductInfoPutHandlerTest extends TestKit(ActorSystem("ProductInfoPutHandlerTest")) with FunSuiteLike with BeforeAndAfterAll{

  val dummyRequest: HttpRequest = new HttpRequest()
  val responder = TestProbe()
  val ctx = new RequestContext(dummyRequest, responder.ref, Uri.Path./)
  val journal = TestProbe()
  val maxWait = 3.seconds
  val putProductInfo = GetProductResponse(100, "Sith", CurrentPrice(999, "Expensive"))
  var actorUnderTest: TestActorRef[ProductInfoPutHandler] = _

  override def beforeAll() = {
    actorUnderTest = TestActorRef[ProductInfoPutHandler](ProductInfoPutHandler.props(ctx, journal.ref, putProductInfo))
  }

  override def afterAll() ={
    system.shutdown()
  }

  test("When Actor is created message is sent to the Journal to request the product information") {
    val res = journal.expectMsgClass(maxWait, classOf[PutProductInfo])

    assert(putProductInfo === res.productInfo)
  }

  test("When receive response from journal then complete the request") {
    val resultMessage = "Welcome My Master"
    val msg = PutProductResponse(resultMessage)

    actorUnderTest ! msg

    val httpResponse = responder.expectMsgClass(maxWait, classOf[HttpResponse])
    val entity = httpResponse.entity.asString(HttpCharsets.`UTF-8`)


    assert(entity.contains(resultMessage))
  }
}
