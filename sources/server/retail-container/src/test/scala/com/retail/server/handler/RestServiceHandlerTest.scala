package com.retail.server.handler

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import com.retail.server.messages.{CurrentPrice, GetProductInfo, GetProductResponse, PutProductInfo}
import org.json4s.DefaultFormats
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuiteLike}
import spray.http.{HttpMethods, HttpRequest, HttpResponse, StatusCodes}

import scala.concurrent.duration._
import org.json4s.native.Serialization.write

class RestServiceHandlerTest extends TestKit(ActorSystem("RestServiceHandlerTest")) with FunSuiteLike with BeforeAndAfterAll with BeforeAndAfterEach with ImplicitSender{
  implicit def json4sFormats = DefaultFormats

  val journal = TestProbe()
  val maxWait = 5.seconds
  val productId =  100
  var actorUnderTest: TestActorRef[RestServiceHandler] =  _

  override def beforeEach() = {
    actorUnderTest =  TestActorRef[RestServiceHandler](RestServiceHandler.props(journal.ref))
  }

  override def afterEach() ={
    system.stop(actorUnderTest)
  }

  override def afterAll() = {
    system.shutdown()
  }

  test("When Get Product info is called actor is created and journal receives message") {
    actorUnderTest ! HttpRequest(HttpMethods.GET, s"/products/$productId")

    assert(actorUnderTest.children.size === 1)

    val res = journal.expectMsgClass(maxWait, classOf[GetProductInfo])
    assert(productId === res.id)
  }

  test("When Put Product is called then actor is created and journal receives message") {
    val entity = GetProductResponse(productId, "Death Star", CurrentPrice(10000, "Galactic credit"))
    actorUnderTest ! HttpRequest(HttpMethods.PUT, s"/products/$productId", entity = write(entity))

    assert(actorUnderTest.children.size === 1)
    val res = journal.expectMsgClass(maxWait, classOf[PutProductInfo])
    assert(entity === res.productInfo)
  }

  test("When initialize is called then no actor is created and journal receives message") {
    actorUnderTest ! HttpRequest(HttpMethods.GET, "/initialize")

    journal.expectMsg(maxWait, 'Initialize)

    val response = expectMsgClass(maxWait, classOf[HttpResponse])
    assert(response.status === StatusCodes.OK)
  }
}
